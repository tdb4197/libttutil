/*
 * Copyright 2019-2022 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "ttutil/ttutil.h"
#include "ttutil/mempool.h"

int tt_mempool_create(struct tt_mempool **pool,
                      const size_t numBlocks, 
                      const size_t blockSize, 
                      const size_t align) {
    assert(NULL != pool);
    assert(0 < numBlocks);
    assert(0 < blockSize);
    assert(TT_POWER_OF_TWO(align));

    *pool = malloc(TT_MEMPOOL_SIZE(align, blockSize, numBlocks));
    if(NULL == *pool) return TT_NOMEM;

    tt_mempool_init(*pool, numBlocks, blockSize, align);

    return TT_SUCCESS;
}

void tt_mempool_init(struct tt_mempool *pool,
                     const size_t numBlocks,
                     const size_t blockSize,
                     const size_t align) {
    assert(NULL != pool);
    assert(0 < numBlocks);
    assert(0 < blockSize);
    assert(TT_POWER_OF_TWO(align));

    // First block starts at first aligned memory location after the mempool struct
    pool->pFirst = TT_ALIGN(align, (uintptr_t)pool + sizeof(struct tt_mempool));
    pool->pNext = pool->pFirst;
    pool->blockSize = TT_MEMPOOL_BLOCKSIZE(align, blockSize);
    pool->pLast = pool->pFirst + ((numBlocks-1)*pool->blockSize);
    pool->numFree = numBlocks;
    pool->numInit = 0;
    pool->numBlocks = numBlocks;
}

void tt_mempool_destroy(struct tt_mempool *pool) {
    assert(NULL != pool);
    free(pool);
}

void* tt_mempool_alloc(struct tt_mempool *pool) {
    assert(NULL != pool);

    // cost of initialization is amortized across calls to tt_mempool_getBlock
    if(pool->numInit < pool->numBlocks) {
        // Calculate address of next uninitialized block and store a pointer to the 
        // following block in it
        uintptr_t p = pool->pFirst + (pool->numInit * pool->blockSize);
        *((uintptr_t*)p) = p + pool->blockSize;
        ++pool->numInit;
    }

    // Get a free block if one is available
    void* ret;
    if(pool->numFree) {
        ret = (void*)pool->pNext;
        --pool->numFree;
        if(pool->numFree) {
            // As initialized above, *pNext points at the next block after itself
            pool->pNext = *(uintptr_t*)pool->pNext;
        } else pool->pNext = (uintptr_t)NULL;
    } else ret = NULL;

    return ret;
}

void tt_mempool_free(struct tt_mempool *pool, void* ptr) {
    assert(NULL != pool);
    assert(NULL != ptr);
    assert((uintptr_t)ptr >= pool->pFirst);
    assert((uintptr_t)ptr <= pool->pLast);
    assert(pool->numFree != pool->numBlocks);

    if(pool->pNext) *(uintptr_t*)ptr = pool->pNext;
    pool->pNext = (uintptr_t)ptr;
    ++pool->numFree;
}
