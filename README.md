# Library of Tom's Tools

Hi. I'm Tom. These are my tools; ttutil is a standalone utility library written
in C for own personal use, education, and amusement. It aims to provide fast,
efficient data structures and algoritms in compact readable form.

# Design Philosophy
1. Code correctness - nothing else matters if it's not correct
2. Readability - the ability to read and understand what the code is doing is
   critical to ensuring code correctness and to maintaining the library.
3. Performance - memory overhead and execution time are always priorities
4. Interesting things - this library was written by me for me. Sometimes I will
   write something a specific way just because it is interesting to me. This is
   why libttutil offers statically allocated data structures, and why tt\_fifo
   uses a goto in tt\_fifo\_write and tt\_fifo\_read to eliminate tail 
   recursion.

# Data Structures
A lot of libraries exist with perfectly functional, mature, well tested data
structure implementations. So what does libttutil have to offer?

## Static Memory Mangement
A major goal for libttutil is to support allocation of data structures into user
provided memory locations. There are a lot of use cases where this is a very
nice thing to be able to do, I will outline a few benefits below:
* Performance - Avoid Heap Contention
  * Multiple theads attempting to perform a malloc or free on an SMP system can
    severly degrade performance.
  * Statically-allocated ttutil data structures own their own memory and do not
    ever call malloc or free, avoiding the global memory lock entirely.
  * The behavior of malloc and free is unpredicatable and can lead to 
    inconsistent performance as well.
* Safety & Compliance - Avoid heap memory allocations
  * Avoiding the use of calloc, malloc, realloc, and free for safety critical
    applications is fairly common, it can elminate a lot of potential issues:
    * Overallocation of platform memory
    * Continuing to use memory after it was freed
    * Failure to free memory
    * Multiple calls to free memory
    * etc.
  * Rule number 3 of Gerard J. Holzmann's "The Power of 10 - Rules for
    Developing Safety Critical Code (http://spinroot.com/gerard/pdf/P10.pdf)
    is "Do not use dynamic memory allocation after initialization". These
    rules have been incorporated in the JPL coding standards.
    * "an upper bound on the use of stack memory can be derived statically,
       thus making it possible to prove that an application will always live
       within its pre-allocated memory"
  * MISRA-C 2004 Rule 20.4: Dynamic heap memory allocation shall not be used.

## Notes
Allocating big blocks of static memory in C is really easy. You can explicitly
specify the section in memory with '\_\_attribute\_\_((section("name")))' but
generally it should be sufficient to declare any static buffers you need at the
top of main and they will be placed into the .bss section. e.g. this code
```C
static uint8_t myBuffer[0x2000];
```
results in an entry like this in your .bss section
```
0000000000000000 l     O .bss	0000000000002000 myBuffer
```

# Testing
Unit testing depends on the [check](https://libcheck.github.io/check/) library.

# License
## BST 3 Clause
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

