/// @file mempool.h
/// @author Thomas D Bottom <tbottom@outlook.com>

/*
 * Copyright 2019-2022 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __TT_MEMORY_POOL_H__
#define __TT_MEMORY_POOL_H__

#include "ttutil/ttutil.h"

// Included in the header for static size calculations, direct access is unwise.
struct tt_mempool {
    uintptr_t pFirst;   // ptr to first block of memory pool
    uintptr_t pNext;    // ptr to next free block
    uintptr_t pLast;    // ptr to last block in memory pool
    unsigned numFree;   // number of free blocks in the pool
    unsigned numInit;   // number of initialize blocks in the pool
    unsigned numBlocks; // total number of blocks in the pool
    unsigned blockSize; // size of a block in bytes
};

// A block in a tt_mempool must be at least as wide as a pointer to allow the
// pool to perform bookkeeping, and may require padding to ensure the block
// size is a multiple of the required alignment
#define TT_MEMPOOL_BLOCKSIZE(a, b) ( TT_MAX( TT_ALIGN((a), sizeof(void*)), \
                                             TT_ALIGN((a), (b))) )

/// @brief calculate the size in bytes necessary to store a tt_mempool with
///        the given parameters.
///
/// tt_mempool memory layout
/// -------------------
/// |struct tt_mempool|
/// | 0<=padding<=a-1 | pad to align block 0
/// |     block 0     | addr % align = 0
/// |       ...       | addr % align = 0
/// |     block n     | addr % align = 0
/// |-----------------|
///
/// 1. The memory address the struct starts at is guaranteed to be suitble for
///    storage of the struct (malloc returns memory suitable for storage of any
///    object and we impose the same requirement on caller-provided memory in
///    tt_mempool_init). 
/// 2. It is not guaranteed to be a multiple of a - it may need
///    up to a-1 padding bytes.
/// 3. block0 is padded to an aligned memory location, and block size is padded
///    to a multiple of a.
///
/// @param a - alignment; must be a power of 2
/// @param b - blockSize
/// @param c - capacity
#define TT_MEMPOOL_SIZE(a,b,c) ( \
    sizeof(struct tt_mempool)                /* 1 */ \
    + (a)                                    /* 2 */ \
    + ((c) * TT_MEMPOOL_BLOCKSIZE((a), (b))) /* 3 */ \
)

/// @brief helper macro to declare memory for a statically allocated tt_mempool
///
/// The resulting struct tt_mempool pointer must be passed to tt_mempool_init
/// before it is ready for use. 
///
/// Most compilers for most platforms will place this in the .bss section. This
/// behavior can be controlled with compiler-specific attributes, e.g. for gcc:
///     __attribute__((section (".bss")))
///
/// @param a - the align parameter passed to tt_mempool_init
/// @param b - the blockSize parameter passed to tt_mempool_init
/// @param c - the capacity parameter passed to tt_mempool_init
/// @param n - the name of the struct tt_mempool to declare
///
/// Example:
///   File-scoped static declaration of mempool:
///     static TT_MEMPOOL(myAlign, sizeof(myStruct), myNumBlocks, myPool);
///
///   Initialize the pool before use:
///     tt_mempool_init(myPool, sizeof(myStruct), myNumBlocks, myAlign);
#define TT_MEMPOOL(a,b,c,n) \
    static uint8_t (n##mpa) [TT_MEMPOOL_SIZE(a,b,c)]; \
    struct tt_mempool * (n) = (struct tt_mempool*) (n##mpa) ;

/// Opaque struct representing a pool of Memory
struct tt_mempool;

/// @brief Create a tt_mempool on the heap and return it in the pointer at *pool
/// 
/// Freeing the resouces used by the tt_mempool requires a call to 
/// tt_mempool_destroy
/// 
/// This method provides a pool of fixed-size blocks of memory. It uses only a
/// single heap allocation, has no loops, and only a small fixed amount of 
/// memory for book-keeping (assuming blockSize%align = 0). Total overhead = 
///   3*sizeof(void*) + 4*sizeof(unsigned) + (align-1) + ((blockSize%align)*numBlocks)
/// 
/// Based on "Fast Efficient Fixed-Size Memory Pool: No Loops and No Overhead"
///     Ben Kenwright (b.kenwright@ncl.ac.uk), Computation Tools 2012
/// 
/// @param pool - pointer to the pointer that will point at the created tt_mempool struct
/// @param numBlocks - the number of blocks to allocate for this tt_mempool
/// @param blockSize - the minimum size of each block (blocks may be larger if necessary
///                    to satisfy the required memory alignment
/// @param align     - each block returned by tt_mempool_alloc is guaranteed to start
///                    at an address which is a multiple of align; sizeof(void*) is a
///                    safe default
/// @return 0 on success, -ENOMEM if the pool could not be allocated
int tt_mempool_create(struct tt_mempool **pool,
                      const size_t numBlocks, 
                      const size_t blockSize, 
                      const size_t align);

/// @brief initialize a tt_mempool in caller-provided memory
///
/// Use TT_MEMPOOL_SIZE to calculate how much space is needed or the convenient
/// TT_MEMPOOL macro to do the static allocation for you.
///
/// @param pool      - pointer to memory to initialize the tt_mempool in
/// @param blockSize - the minimum size of each block (blocks may be larger if necessary
///                    to satisfy the required memory alignment
/// @param numBlocks - the number of blocks to allocate for this tt_mempool
/// @param align     - each block returned by tt_mempool_alloc is guaranteed to start
///                    at an address which is a multiple of align; sizeof(void*) is a
///                    safe default
/// @return 0 on success, -ENOMEM if the pool could not be allocated
void tt_mempool_init(struct tt_mempool *pool,
                     const size_t numBlocks,
                     const size_t blockSize, 
                     const size_t align);

/// @brief Frees all resources associated with a tt_mempool created with
///        tt_mempool_create.
///
/// Never call this on a statically allocated tt_mempool.
///
/// @param pool - the pool to destroy. Must be a valid pool allocated by
///               tt_mempool_create. Multiple calls with the same pointer or
///               passing a NULL pointer will result in an error.
void tt_mempool_destroy(struct tt_mempool *pool);

///
/// @brief Allocate a block of memory from the tt_mempool.
/// @param pool - the tt_mempool to allocate a block of memory from
/// @return - a pointer to a free block of at least as many bytes as the blockSize
///           parameter passed to tt_mempool_create, or NULL if there are no free blocks
void* tt_mempool_alloc(struct tt_mempool *pool);

///
/// @brief Free the passed in block of memory, returning it to the tt_mempool
/// @param pool - the pool to return the block of memory to. This must be the same pool
///               the block was alloc'd from.
/// @param ptr - the block of memory to return to the pool.
void tt_mempool_free(struct tt_mempool *pool, void* ptr);

#endif
