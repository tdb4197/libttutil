/*
 * Copyright 2019 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __TT_UTIL_QUEUE_H__
#define __TT_UTIL_QUEUE_H__

/**
 * @brief Struct containing the internal state of a tt_queue.
 * Allocated by tt_queue_create and deallocated by calling free() on the
 * pointer provided by tt_queue_create.
 */
struct tt_queue;

/**
 * @brief Create a fixed-size queue which can store capacity objects of slotSize bytes
 * @param queue - address of caller tt_queue pointer. The address of the created
 *                struct will be written to *queue.
 * @param capacity - the number of slots in the queue
 * @param slotSize - size in bytes of each slot in the queue
 * @return TT_NOMEM if the queue could not be allocated, TT_SUCCESS otherwise
 */
int tt_queue_create(struct tt_queue ** queue, unsigned capacity, unsigned slotSize);

/**
 * @brief Push data onto the queue
 * @param queue - the queue to push data onto
 * @param src - pointer to data of size slotSize to enqueue
 * @return - TT_FULL if there is no room in the queue, TT_SUCCESS otherwise
 */
int tt_queue_push(struct tt_queue *queue, void *src);

/**
 * @brief Pop data from the queue and return it in *dest.
 * @param queue - the queue
 * @param dest  - pointer to space of at least size slotSize to return dequeued data to
 * @return TT_EMPTY if there is nothing in the queue, TT_SUCCESS otherwise
 */
int tt_queue_pop(struct tt_queue *queue, void *dest);

#endif
