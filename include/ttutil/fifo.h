/*
 * Copyright 2019 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __TT_FIFO__
#define __TT_FIFO__

/**
 * This struct contains the state of a generic ring-buffer fifo.
 * The fifo is allocated by calling tt_fifo_create and can be
 * deallocated by calling free() on the returned pointer.
 */
struct tt_fifo;

/**
 * @brief Create a generic fifo of size bytes.
 * @param fifo - address of caller fifo pointer. The address of the created
 *               stuct fifo will be written to *fifo. It is the caller's
 *               responsibility to call free() on this pointer.
 * @param size - the size in bytes of the allocated fifo.
 * @return - 0 on success, -ERRNO otherwise.
 */
int tt_fifo_create(struct tt_fifo **fifo, size_t size);

/**
 * @brief Write up to len bytes from src to fifo. Less bytes will
 *        be written if the fifo does not have sufficient space.
 * @param fifo - the fifo to write to
 * @param src  - the data to write to the fifo
 * @param len  - the number of bytes to write
 * @return - the number of bytes written to fifo, or -ERRNO on failure
 */
int tt_fifo_write(struct tt_fifo *fifo, void* src, size_t len);

/**
 * @brief Read up to len bytes from fifo to dest.
 * @param fifo - the fifo to read from
 * @param dest - the destination to read to
 * @param len  - the number of bytes to read
 * @return - the number of bytes read into dest, or -ERRNO on failure
 */
int tt_fifo_read(struct tt_fifo *fifo, void* dest, size_t len);

#endif
