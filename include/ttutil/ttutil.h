#ifndef __TT_UTIL_H__
#define __TT_UTIL_H__

#include <stdbool.h>

enum tt_errno {
    TT_SUCCESS,
    TT_NOMEM,
    TT_FULL,
    TT_EMPTY,
    TT_BADARG
};

inline uint32_t tt_rotl32(uint32_t x, int8_t r)
{
  return (x << r) | (x >> (32 - r));
}

inline uint64_t tt_rotl64(uint64_t x, int8_t r)
{
  return (x << r) | (x >> (64 - r));
}

#define TT_POWER_OF_TWO(x) ( ((x) != 0) && ((x) & ((x)-1)) == 0 )
#define TT_MAX(a,b) ( (b) > (a) ? (b) : (a) )

// Return the smallest value >= b such that b % a = 0
#define TT_ALIGN(a,b) ( ((b)%(a)) == 0 ? (b) : ((b)+(a)-((b)%(a))) )

#endif
