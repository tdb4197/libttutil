/*
 * Copyright 2022 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __TT_UTIL_LIST_H__
#define __TT_UTIL_LIST_H__

// direct access unwise
struct tt_list {
    struct tt_mempool *pool;
    struct tt_list_node *head;
    struct tt_list_node *tail;
    size_t size;
    size_t capacity;
    size_t elementSize;
};
struct tt_list_node;

/// @brief Initialize a fixed capacity tt_list and allocate heap storage for it
///
/// @param list        - tt_list to initialize
/// @param capacity    - maximum number of elements this list can store
/// @param elementSize - the size in bytes of each element to be stored in the 
///                      list
///
/// @return            - TT_NOMEM if the list could not be created,
///                      TT_SUCCESS otherwise
void tt_list_init(struct tt_list *list, 
                  const size_t elementSize,
                  const size_t capacity, 
                  const size_t align);

/// @brief get a pointer to the head of the list
struct tt_list_node* tt_list_head(struct tt_list *list);

/// @brief get a pointer to the tail of the list
/// @param list - the list to get the tail of
/// @return - the tail of he list, or NULL if list is empty
struct tt_list_node* tt_list_tail(struct tt_list *list);

/// @brief get the number of elements in the list
/// @param list - the list to get the size of
/// @return the number of elements in the list
size_t tt_list_size(struct tt_list *list);

/// @brief get the maximum capacity of he list
/// @param list - the list to get the capacity of
/// @return the capacity of the list
size_t tt_list_capacity(struct tt_list *list);

/// @brief get the size in bytes of list elements (stored data)
/// @param list - the list to get the elementSize of
/// @return the size in bytes of list elements
size_t tt_list_element_size(struct tt_list *list);

/// @brief place a new list node at the head of the list and copy data to it
/// @param list - the list to prepend to
/// @param data - the data to copy into the new list node. If NULL is passed
///               for this parameter a node with uninitialized data will be
///               added to the list.
/// @return     - TT_FULL if the list is at capacity, TT_SUCCESS otherwise.
int tt_list_prepend(struct tt_list *list, void *data);

/// @brief place a new list node at the tail of the list and copy data to it
/// @param list - the list to apppend to
/// @param data - the data to copy into the new list node. If NULL is passed
///               for this parameter a node with uninitialized data will be
///               added to the list.
/// @return     - TT_FULL if the list is at capacity, TT_SUCCESS otherwise.
int tt_list_append(struct tt_list *list, void *data);

/// @brief erase node from list
/// @param list - the list to erase node from
/// @param node - the node to erase from list
void tt_list_erase(struct tt_list *list, struct tt_list_node *node);

/// @brief get a pointer to the data stored in node
/// @param node - the node to get the data pointer from
/// @return data pointer
void* tt_list_get(struct tt_list_node *node);

#endif
