#include <assert.h>  // assert
#include <stdbool.h> // bool
#include <stddef.h>  // size_t
#include <stdint.h>  // uintptr_t
#include <stdlib.h>  // malloc
#include <string.h>  // memcpy

#include "ttutil/ttutil.h"
#include "ttutil/mempool.h"
#include "ttutil/list.h"

struct tt_list_node {
    struct tt_list_node *next;
    struct tt_list_node *prev;
};

// space is allocated immediately after prev 
// TODO - alignment stuff
#define NODE_DPTR(node) ((void*)((uintptr_t)&(node)->prev + sizeof((node)->prev)))

int tt_list_create(struct tt_list **list,
                   const size_t capacity,
                   const size_t elementSize,
                   const size_t align) {
    // sanity checks
    assert(NULL != list);
    assert(0 < capacity);
    assert(0 < elementSize);
    assert(TT_POWER_OF_TWO(align));

    //single heap allocation
    const size_t listSize = sizeof(struct tt_list) + 
                            align + // padding
                            TT_MEMPOOL_SIZE(align, elementSize, capacity);
    *list = malloc(listSize);
    if(NULL == *list) return TT_NOMEM;

    tt_list_init(*list, elementSize, capacity, align);

    return TT_SUCCESS;
}

/// @brief 
///
/// @param list        - tt_list to initialize
/// @param capacity    - maximum number of elements this list can store
/// @param elementSize - the size in bytes of each element to be stored in the list
/// @param align TODO
///
/// @return            - TT_NOMEM if the list could not be created, TT_SUCCESS otherwise
void tt_list_init(struct tt_list *list, 
                  const size_t elementSize,
                  const size_t capacity, 
                  const size_t align) {
    assert(NULL != list);
    assert(0 < capacity);
    assert(0 < elementSize);
    assert(TT_POWER_OF_TWO(align));

    // pool starts at first aligned location in memory after struct tt_list
    list->pool = (struct tt_mempool*) 
        TT_ALIGN(align, (uintptr_t)list + sizeof(struct tt_list));
    tt_mempool_init(list->pool, elementSize, capacity, align);

    // Initialize
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    list->capacity = capacity;
    list->elementSize = elementSize;
}

struct tt_list_node* tt_list_head(struct tt_list *list) {
    return list->head;
};

struct tt_list_node* tt_list_tail(struct tt_list *list) {
    return list->tail;
};

size_t tt_list_size(struct tt_list *list) {
    return list->size;
};

size_t tt_list_capacity(struct tt_list *list) {
    return list->capacity;
};

size_t tt_list_element_size(struct tt_list *list) {
    return list->elementSize;
};

// Copy data to a new list node and prepend it to the list
int tt_list_prepend(struct tt_list *list, void *data) {
    struct tt_list_node *dest = tt_mempool_alloc(list->pool);
    if(NULL == dest) return TT_FULL; // pool full -> list full
    if(NULL != data) memcpy(NODE_DPTR(dest), data, list->elementSize);

    dest->prev = NULL; // prepend so prev is always NULL

    if(NULL == list->head) {
        list->head = dest;
        list->tail = dest;
        dest->next = NULL;
    } else {
        list->head->prev = dest;
        dest->next = list->head;
        list->head = dest;
    };

    list->size += 1;
    return TT_SUCCESS;
}

int tt_list_append(struct tt_list *list, void *data) {
    struct tt_list_node *dest = tt_mempool_alloc(list->pool);
    if(NULL == dest) return TT_FULL;
    if(NULL != data) memcpy(NODE_DPTR(dest), data, list->elementSize);

    dest->next = NULL; // append to next is always NULL

    if(NULL == list->tail) {
        list->head = dest;
        list->tail = dest;
        dest->prev = NULL;
    } else {
        list->tail->next = dest;
        dest->prev = list->tail;
        list->tail = dest;
    }

    list->size += 1;
    return TT_SUCCESS;
}

int tt_list_insert(struct tt_list *list, struct tt_list_node *node, void *data) {
    struct tt_list_node *dest = tt_mempool_alloc(list->pool);
    if(NULL == dest) return TT_FULL;
    if(NULL != data) memcpy(NODE_DPTR(dest), data, list->elementSize);
    dest->next = node->next;
    node->next = dest->next;
    dest->next->prev = dest;
    dest->prev = node;
    list->size += 1;
}

// erase node from list
void tt_list_erase(struct tt_list *list, struct tt_list_node *node) {
    if(NULL == node->prev) {
        list->head = node->next;
    } else {
        node->prev->next = node->next;
    }

    if(NULL == node->next) {
        list->tail = node->prev;
    } else {
        node->next->prev = node->prev;
    }

    list->size -= 1;

    // return the node to the pool
    tt_mempool_free(list->pool, node);
}

// get data pointer from tt_list_node
void* tt_list_data(struct tt_list_node *node) {
    return NODE_DPTR(node);
}

struct mergelist {
    struct tt_list_node *head;
    struct tt_list_node *tail;
};
static struct mergelist tt_list_mergesort(struct tt_list_node *head, 
                                          const size_t len, 
                                          bool (*less)(void *a, void *b) ) {
    struct mergelist ret;

    if(len > 1) {
        struct tt_list_node *list1 = head;
        struct tt_list_node *list2 = head;
        const size_t list1len = len / 2;
        const size_t list2len = len - list1len;

        // seek to middle of list1 to find head of list2
        for(size_t i = 0; i < list1len; ++i) list2 = list2->next;

        // split
        list2->prev->next = NULL;
        list2->prev = NULL;

        // recurse
        struct mergelist r1 = tt_list_mergesort(list1, list1len, less);
        struct mergelist r2 = tt_list_mergesort(list2, list2len, less);

        // init sorted list head
        if(less(NODE_DPTR(r1.head), NODE_DPTR(r2.head))) {
            ret.head = r1.head;
            r1.head = r1.head->next;
        } else {
            ret.head = r2.head;
            r2.head = r2.head->next;
        }
        ret.head->prev = NULL;

        // merge
        struct tt_list_node *current = ret.head;
        while(true) {
            if(r1.head != NULL) {
                if(r2.head == NULL || less(NODE_DPTR(r1.head), 
                                           NODE_DPTR(r2.head))) {
                    current->next = r1.head;
                    current->next->prev = current;
                    current = r1.head;
                    r1.head = r1.head->next;
                } else {
                    current->next = r2.head;
                    current->next->prev = current;
                    current = r2.head;
                    r2.head = r2.head->next;
                }
            } else if(r2.head != NULL) {
                current->next = r2.head;
                current->next->prev = current;
                current = r2.head;
                r2.head = r2.head->next;
            } else {
                current->next = NULL;
                ret.tail = current;
                break;
            }
        }
        ret.tail = current;
    } else {
        ret.head = head;
        ret.tail = head;
    }
    return ret;
}

// mergesort the list
void tt_list_sort(struct tt_list *list, bool (*less)(void *a, void *b)) {
    struct mergelist sorted = tt_list_mergesort(list->head, list->size, less);
    list->head = sorted.head;
    list->tail = sorted.tail;
}

