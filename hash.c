struct tt_hashtable {
    size_t size;
    size_t capacity;
    size_t elementSize;
    size_t keySize;
    size_t bucketSize;
    size_t elementOffset;
    void   *buckets;
    uint32_t (*hashfunc)(void* data, size_t len);
};

struct tt_hashtable_node {
    void *key;
    void *data;
};

#define TT_HASHTABLE_GET_DPTR(node) // TODO





// IDEA - 
// Hashtable is backed by a tt_mempool for data storage and
// an array of pointers representing buckets, initially all null
//
// Allocate block from mempool, return TT_FULL on failure
// Hash key
// If buckets[hash(key)%capacity] is null store ptr there
// else while(bucket->next != NULL) bucket = bucket->next;
//
// Memory layout for bucket:
// sizeof(void*) - next pointer
// 0 <= pad <= align-1 padding bytes
// keySize key bytes
// 0 <= pad <= align-1 padding bytes
// elementSize data bytes
//
// Since the buckets are managed by tt_mempool the full layout in memory looks
// like this:
//  
//  ------------------------
//  | struct tt_hashtable  |
//  ------------------------
//  | bucket ptr array     |
//  ------------------------
//  | struct tt_mempool    |
//  ------------------------
//  | pad to align block 0 |
//  ------------------------
//  |       block 0        |
//  ------------------------
//  | pad to align block 1 |
//  ------------------------
//  |       block 1        |
//  ------------------------
//  |         ...          |
//  ------------------------
//  | pad to align block n |
//  ------------------------
//  |       block n        |
//  ------------------------
// 
// Each of the buckets is effectively a node in a singly linked list. Since the tt_mempool
// is keeping track of the free blocks of memory, this allows the hashtable to get linked-list
// collision handling performance without using dynamic memory. Neat.
//
// Overhead = capacity * (sizeof(void*) +
//                        per_bucket_alignment_padding +
//                        per_mempool_block_alignment_padding )
//   

// TODO - macro for TT_HASHTABLE_SIZE and TT_HASHTABLE static allocation

struct tt_hashtable_bucket {
    void *key;
    void *data;
    struct tt_hashtable_bucket *next;
};

struct tt_hashtable {
    size_t capacity;
    size_t elementSize;
    size_t keySize;
    size_t bucketSize;
    size_t align;
    size_t size;
    uint32_t (*hashfunc)(void* data, size_t len);
    struct tt_mempool *pool; 
    struct tt_hashtable_bucket *buckets;
};

// Bucket:
// keySize bytes     KEY
// padding
// elementSize bytes DATA
// padding
// next pointer
#define TT_HASHTABLE_BUCKET_SIZE(align, keySize, elementSize) ( \
    TT_ALIGN((keySize), (align))     + \
    TT_ALIGN((elementSize), (align)) + \
    sizeof(void*)                      \
)


#define TT_HASHTABLE_SIZE(a, kb, eb, c) (                                \
    sizeof(struct tt_hashtable) +                                        \
    TT_MEMPOOL_SIZE((a), TT_HASHTABLE_BUCKET_SIZE((a), (kb), (eb)), (c)) \
)

// The extra effort of these offset calculations could be avoided
// by changing the memory layout of the buckets to be
//
// struct tt_hashtable_bucket {
//    struct tt_hashtable_bucket *next;
//    void *key;
//    void *data;
// };
//
// *next - null or next struct_tt_hashtable in collision chain
// *key  - points to KEY below
// *data - points to DATA below
// PADDING
// KEY
// PADDING
// DATA
//
// Downside is an extra 2*sizeof(void*) bytes of memory used
// Upside is these ugly macros go away and avoid having to some extra
// memory reads and additions
//
//
// Another idea - why not just store the key and element offsets? then total
// overhead is just 2*sizeof(size_t) and getting the pointers becomes:
// next = table->buckets + (table->bucketSize * index)
// key  = table->buckets + (table->bucketSize * index) + table->keyOffset
// data = table->buckets + (table->bucketsize * index) + table->elementOffset
//
//
// Used as return-container for users
struct tt_hashtable_node {
    void* key;
    void* data;
    struct tt_hashtable *table; // convenience ptr back to table
};


// Convenience macros for accessing buckets
#define TT_HASHTABLE_KPTR(table, bucket) ((void*) \
    (((uintptr_t)(bucket)) + (table)->keyOffset)  \
)
#define TT_HASHTABLE_DPTR(table, bucket) ((void*) \
    (((uintptr_t)(bucket)) + (table)->dataOffset) \
)
#define TT_HASHTABLE_NPTR(table, bucket) ((void*) \
    (((uintptr_t)(bucket)) + (table)->nextOffset) \
)

struct tt_hashtable_node* tt_hashtable_begin(struct tt_hashtable *table) {
    assert(NULL != table);

    if(0 == table->size) return tt_hashtable_end(table);

    // Find the first non-null bucket ptr
    bucketptr_t bucket = NULL;
    for(size_t i = 0; NULL == bucket && i < table->capacity; ++i) {
        bucket = table->buckets[i];
    }
    assert(NULL != bucket);

    struct tt_hashtable_node ret;
    ret.key  = TT_HASHTABLE_KPTR(table, bucket);
    ret.data = TT_HASHTABLE_DPTR(table, bucket;

    assert(NULL != ret.key);
    assert(NULL != ret.data);
    return ret;
}

struct tt_hashtable_node* tt_hashtable_end(struct tt_hashtable_node *node) {
    return NULL;
}

struct tt_hashtable_node* tt_hashtable_next(struct tt_hashtable_node *node) {
    assert(NULL != node);
    assert(NULL != node->table);

    const tt_hashtable *table = node->table;
    const uint32_t hash = table->hashfunc(node->key, node->table->keySize);
    bucketptr_t bucket = NULL;
    for(size_t i = (hash%table->capacity); 
        bucket == NULL && i < table->capacity; 
        ++i) 
    {
        bucket = table->buckets[i];
    }

    if(NULL == bucket) return tt_hashtable_end(table);
    
    struct tt_hashtable_node ret;
    ret.key  = TT_HASHTABLE_KPTR(table, b);
    ret.data = TT_HASHTABLE_DPTR(table, b)

    assert(NULL != ret.key);
    assert(NULL != ret.data);
    // TODO - assert pointers are in bounds

    return ret;
}

struct tt_hashtable_node* tt_hashtable_at(struct tt_hashtable *table,
                                          void *key) {
    assert(NULL != table);
    assert(NULL != key);

    const uint32_t       hash = table->hashfunc(key, table->keySize);
    const size_t        index = hash % table->capacity;
    const bucketptr_t  bucket = table->buckets[index];

    if(NULL == bucket) return tt_hashtable_end(table);

    struct tt_hashtable_node ret;
    ret.key  = TT_HASHTABLE_KPTR(table, bucket);
    ret.data = TT_HASHTABLE_DPTR(table, bucket);

    assert(NULL != ret.key);
    assert(NULL != ret.data);

    return ret;
}

bool tt_hashtable_contains(struct tt_hashtable *table, void *key) {
    assert(NULL != table);
    assert(NULL != key);

    const uint32_t       hash = table->hashfunc(key, table->keySize);
    const size_t        index = hash % table->capacity;
    const bucketptr_t  bucket = table->buckets[index];

    return (NULL == bucket);
}


bool tt_hashtable_erase(struct tt_hashtable *table,void *key) {

}

bool tt_hashtable_insert(struct tt_hashtable *table, void *key, void *data) {

}


//
// struct tt_hashtable_node gets pared down to just the next pointer
// users never interact with nodes, just make calls to:
// contains(key)
// data(key)
// remove(key)
// tt_hashtable_node* begin() -> pointer to first non-null element
// tt_hashtable_node* end()   -> pointer to lastblock + 1
// tt_hashtable_next* next()  -> linear search over buckets array to find
//                               next non-null bucket ptr
// void* tt_hashtable_key(struct tt_hashtable *table,
//                        struct tt_hashtable_node *node);
// void* tt_hashtable_data(struct tt_hashtable_node *node);
// struct tt_hashtable_node* tt_hashtable_next(struct tt_hashtable *table,
//                                             struct tt_hashtable_node *node);
// struct tt_hashtable_node* tt_hashtable_begin(struct tt_hashtable *table);
// struct tt_hashtable_node* tt_hashtable_end(struct tt_hashtable *table);
//
#define TT_HASHTABLE_GET_KPTR(table, index) (                              \
    (void*)(((uintptr_t)(table)->buckets) + ((table)->bucketSize*(index))) \
)

#define TT_HASHTABLE_GET_DPTR(table, index) (                      \
    (void*)(((uintptr_t)TT_HASHTABLE_GET_KPTR((table), (index))) + \
    TT_ALIGN((table)->align, (table)->keySize))                    \
)

#define TT_HASHTABLE_GET_NPTR(table, index) (                      \
    (void*)(((uintptr_t)TT_HASHTABLE_GET_DPTR((table), (index))) + \
    TT_ALIGN((table->align), (table)->elementSize))                \
)

void tt_hashtable_init(struct tt_hashtable *table,
                       const size_t capacity,
                       const size_t elementSize,
                       const size_t keySize,
                       const size_t align
                       uint32_t (*hashfunc)(void* data, size_t len)) {
    assert(NULL != table);
    assert(0 < table->capacity);
    assert(0 < table->elementSize);
    assert(0 < table->keySize);
    assert(TT_POWER_OF_TWO(align));
    assert(NULL != hashfunc);

    table->capacity = capacity;
    table->elementSize = elementSize;
    table->keySize = keySize;
    table->align = align;
    table->bucketSize = TT_HASHTABLE_BUCKET_SIZE(keySize, elementSize, align);
    table->size = 0;
    table->hashfunc = hashfunc;

    table->pool = (uintptr_t)table + sizeof(struct tt_hashtable);
    assert(0 == table->pool % alignof(struct tt_mempool));
    tt_mempool_init(table->pool);

    table->buckets = TT_ALIGN(
        (uintptr_t)table->pool + TT_MEMPOOL_SIZE(align, elementSize, capacity),
        alignof(struct tt_hashtable_bucket*));
    memset(table->buckets, 0, sizeof(void*) * capacity); // NULL = empty bucket

    // postconditions for correctly initialized empty tt_hashtable
    // - check correct alignment of manually-placed structures
    assert(0 == ((uintptr_t)table->pool) % alignof(struct tt_mempool));
    assert(0 == ((uintptr_t)table->buckets % alignof(struct tt_hashtable_bucket)));
}

int tt_hashtable_create(struct tt_hashtable **table,
                        const size_t capacity,
                        const size_t elementSize,
                        const size_t keySize,
                        const size_t align
                        uint32_t (*hashfunc)(void* data, size_t len)) {
    assert(NULL != table);
    assert(0 < table->capacity);
    assert(0 < table->elementSize);
    assert(0 < table->keySize);
    assert(TT_POWER_OF_TWO(align));
    assert(NULL != hashfunc);

    *table = malloc(TT_HASHTABLE_SIZE(align, elementSize, keySize, capacity));
    if(NULL == *table) return TT_FULL;

    tt_hashtable_init(*table, capacity, elementSize, keySize, align, hashfunc);

    return TT_SUCCESS;
}






















/// @brief - "placement create" a new tt_hashtable in the provided memory
///
/// @param table - double-pointer used to return memory address of tt_hashtable
/// @param capacity - max number of elements the tt_hashtable can store
/// @param elementSize - the size of each element in bytes
/// @param keySize - the size of each key in bytes
/// @param buf - pointer to the raw memory the tt_hashtable will be placed in
/// @param bufLen - the length of buf
/// @param align - each element stored in the table is guaranteed to be aligned to a memory
///                boundary which is a multiple of this number. Must be a power of 2.
/// @param hashfunc - hash function to be used for calculating keys
/// @return on success the number of bytes of the buffer consumed by the tt_hashtable
///         on failure a negative tt_errno is returned
int tt_hashtable_pcreate(struct tt_hashtable **table, 
                         size_t capacity, 
                         size_t elementSize, 
                         size_t keySize;
                         void* buf, 
                         size_t bufLen, 
                         size_t align,
                         uint32_t (*hashfunc)(void* data, size_t len)) {
    struct tt_hashtable *table;
    int result;
    int consumed = 0;
    size_t bucketSize;

    // Initialize the tt_hashtable in the provided buffer
    *table = buf;
    buf = (uintptr_t)buf + sizeof(struct tt_hashtable);
    consumed += sizeof(struct tt_hashtable);
    list->hashfunc = hashfunc;
    list->size = 0;
    list->capacity = capacity;
    list->elementSize = elementSize;
    list->keySize = keySize;

    // Calculate size of each bucket
    // Contents:
    //   n-byte key
    //   m-byte value

    // Hash table buckets start at first aligned memory address after the tt_hashtable struct
    result = tt_align_offset(buf, align);
    list->buckets = (uintptr_t)buf + result;
    buf = (uintptr_t)buf + result;
    consumed += result;

    // Calculate the padded size of a bucket so the next bucket falls on an align-byte boundary
    list->elementOffset = keySize + tt_align_offset(keySize, align);
    list->bucketSize    = keySize + list->elementOffset + elementSize;
    list->bucketSize   += tt_align_offset(list->bucketSize, align);
    memset(buckets, 0, list->bucketSize * capacity);

    consumed += capacity * list->bucketSize;

    return consumed;
}





























struct tt_list {
    struct tt_list_node *head;
    struct tt_list_node *tail;
    size_t size;
    size_t elementSize;
    size_t capacity;
    struct tt_mempool pool;
};

struct tt_list_node {
    struct tt_list_node *next;
    struct tt_list_node *tail;
    void *data;
};

void tt_list_init(struct tt_list *list, size_t elementSize, size_t capacity) {
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    list->elementSize = elementSize;
    list->capacity = capacity;

    // tt_list is backed by a tt_mempool
    //   tt_mempool is a convenience class for working with fixed sized
    //   blocks of memory that in addition to being very convenient to
    //   use in this context also frees tt_list from needing to acquire
    //   the global memory lock to allocate and free list nodes.
    size_t blockSize = sizeof(struct tt_list_node) + elementSize;
    if(capacity == 0) {
        // TODO - dynamic mempool
    } else {
        // TODO - fixed mempool
    }
}

struct tt_list_node * tt_list_begin(struct tt_list *list) {
    return list->head;
}

bool tt_list_append(struct tt_list *list, void *data) {
    
}













