/*
 * Copyright 2020 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h> // malloc
#include <stdint.h> // uint8_t
#include <string.h> // memcpy

#include "ttutil/ttutil.h"

struct tt_queue {
    uint8_t *start;  // ptr to start of the queue
    size_t slotSize; // size of each slot in bytes
    size_t capacity; // total number of slots
    size_t head;     // idx of next slot to read from
    size_t tail;     // idx of next slot to write to
    size_t count;    // count of full slots
};

int tt_queue_create(struct tt_queue ** queue, unsigned capacity, unsigned slotSize) {
    *queue = malloc(sizeof(struct tt_queue) + (capacity * slotSize));
    if(NULL == *queue) return TT_NOMEM;

    (*queue)->start = (uint8_t*)*queue + sizeof(struct tt_queue);
    (*queue)->slotSize = slotSize;
    (*queue)->capacity = capacity;
    (*queue)->head  = 0;
    (*queue)->tail  = 0;
    (*queue)->count = 0;

    return TT_SUCCESS;
}

int tt_queue_push(struct tt_queue *queue, void *src) {
    if(queue->capacity == queue->count) return TT_FULL;

    uint8_t *dest = queue->start + (queue->slotSize * queue->tail);
    memcpy(dest, src, queue->slotSize);
    queue->tail = (queue->tail+1) % queue->capacity;
    ++queue->count;

    return TT_SUCCESS;
}

int tt_queue_pop(struct tt_queue *queue, void *dest) {
    if(0 == queue->count) return TT_EMPTY;

    uint8_t *src = queue->start + (queue->slotSize * queue->head);
    memcpy(dest, src, queue->slotSize);
    queue->head = (queue->head+1) % queue->capacity;
    --queue->count;

    return TT_SUCCESS;
}
