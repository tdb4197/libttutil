#include <stdlib.h>
#include <check.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "ttutil/ttutil.h"
#include "ttutil/queue.h"

#define QUEUE_SIZE 128
START_TEST(test_queue) {
    fprintf(stderr, "Start test_queue\n");

    struct tt_queue *queue;
    int result = tt_queue_create(&queue, QUEUE_SIZE, sizeof(unsigned));
    ck_assert(0 == result);

    unsigned numFree = QUEUE_SIZE;
    unsigned nextPop = 0;
    unsigned nextPush = 0;

    for(int i = 0; i < 10000; ++i) {
        // Randomly choose to read or write
        unsigned read = rand() % 2;

        if(read) {
            unsigned pop;
            result = tt_queue_pop(queue, &pop);

            if(numFree == QUEUE_SIZE) ck_assert(result == TT_EMPTY);
            else {
                ck_assert(result == TT_SUCCESS);
                ck_assert(pop == nextPop);
                ++nextPop;
                ++numFree;
            }
        } else {
            result = tt_queue_push(queue, &nextPush);
            if(numFree == 0) ck_assert(result == TT_FULL);
            else {
                ck_assert(result == TT_SUCCESS);
                ++nextPush;
                --numFree;
            }
        }
    }

    free(queue);
}
END_TEST


Suite * queue_suite(void) {
    Suite *s;
    TCase *tc_core;

    s = suite_create("queue");

    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_queue);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int numFailed;
    Suite *s;
    SRunner *sr;

    srand(time(0));

    s = queue_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    numFailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return numFailed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
