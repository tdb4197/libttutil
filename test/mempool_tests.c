#include <stdlib.h>
#include <check.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "ttutil/mempool.h"

struct TestBox {
    int a;
    int b;
    int c;
};

TT_MEMPOOL(8,sizeof(struct TestBox), 32, foobar);

START_TEST(test_mempool_allocfree) {
    struct tt_mempool *pool;

    fprintf(stderr, "sizeof(foobar) = %u\n", sizeof(foobar));
    fprintf(stderr, "sizeof(foobarmpa) = %u\n", sizeof(foobarmpa));

    int result = tt_mempool_create(&pool, // return the mempool* to pool
                                   32, // 32 blocks
                                   sizeof(struct TestBox),
                                   8); // aligned to 8 byte boundaries

    ck_assert(result == 0);
    ck_assert(NULL != pool);

    struct TestBox* boxPtrs[32];
    memset(boxPtrs, 0, 32*sizeof(uintptr_t));

    int numFree = 32;
    for(int i = 0; i < 10000; ++i) {
        int idx = rand() % 32;

        // Allocate a new TestBox or validate and free and old one
        if(NULL == boxPtrs[idx]) {
            boxPtrs[idx] = tt_mempool_alloc(pool);
            if(0 == numFree) {
                ck_assert(NULL == boxPtrs[idx]);
            } else {
                ck_assert(NULL != boxPtrs[idx]);
                boxPtrs[idx]->a = idx;
                boxPtrs[idx]->b = idx*2;
                boxPtrs[idx]->c = idx*8;
                --numFree;
            }
        } else {
            ck_assert(boxPtrs[idx]->a == idx);
            ck_assert(boxPtrs[idx]->b == idx*2);
            ck_assert(boxPtrs[idx]->c == idx*8);
            tt_mempool_free(pool, boxPtrs[idx]);
            boxPtrs[idx] = NULL;
            ++numFree;
        }
    }

    tt_mempool_destroy(pool);
}
END_TEST

Suite * mempool_suite(void) {
    Suite *s;
    TCase *tc_core;

    s = suite_create("mempool");

    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_mempool_allocfree);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int numFailed;
    Suite *s;
    SRunner *sr;

    srand(time(0));

    s = mempool_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    numFailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return numFailed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
