#include <stdlib.h>
#include <check.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "ttutil/fifo.h"

struct TestBox {
    int a;
    int b;
    int c;
};

START_TEST(test_fifo_overwrite) {
    struct tt_fifo *fifo;

    uint8_t buf[1024];

    int result = tt_fifo_create(&fifo, 4);
    ck_assert(0 == result);
    
    result = tt_fifo_write(fifo, buf, 1024);
    ck_assert(4 == result);

    free(fifo);
}END_TEST

START_TEST(test_fifo_overread) {
    struct tt_fifo *fifo;

    uint8_t buf[1024];

    int result = tt_fifo_create(&fifo, 4);
    ck_assert(0 == result);
    
    result = tt_fifo_write(fifo, buf, 1024);
    fprintf(stderr, "writeResult = %d\n", result);
    ck_assert(4 == result);

    result = tt_fifo_read(fifo, buf, 1024);
    fprintf(stderr, "readResult = %d\n", result);
    ck_assert(4 == result);

    free(fifo);

}END_TEST

#define FIFO_SIZE 512
START_TEST(test_fifo) {
    fprintf(stderr, "Start test_fifo\n");
    struct tt_fifo *fifo;

    int bytes_free = FIFO_SIZE;
    uint8_t writeBuf[FIFO_SIZE];
    uint8_t readBuf[FIFO_SIZE];
    uint8_t lr = 0;
    uint8_t lw = 0;

    // 4 byte fifo
    int result = tt_fifo_create(&fifo, FIFO_SIZE);
    ck_assert(0 == result);

    for(int i = 0; i < 1000; ++i) {
        // Randomly choose to read or write
        unsigned read = rand() % 2;

        if(read) {
            size_t readTry = (rand() % FIFO_SIZE) + 1; // up to fifo size bytes
            size_t realRead = (readTry > (FIFO_SIZE - bytes_free) ? (FIFO_SIZE - bytes_free) : readTry);

            result = tt_fifo_read(fifo, readBuf, readTry);
            ck_assert(result == realRead);
            bytes_free += result;
            
            // Validate each byte read is what we expect it to be
            for(int j = 0; j < result; ++j) {
                ck_assert(lr == readBuf[j]);
                ++lr;
            }
        } else {
            size_t writTry = (rand() % FIFO_SIZE) + 1; // up to fifo size bytes

            size_t realWrite = (writTry > bytes_free ? bytes_free : writTry);
            for(int j = 0; j < realWrite; ++j) {
                writeBuf[j] = lw;
                ++lw;
            }

            result = tt_fifo_write(fifo, writeBuf, writTry);
            ck_assert(result == realWrite);
            bytes_free -= result;
        }
    }

    free(fifo);
}
END_TEST

// Test case makes uses of FIFO as a queue of unsigned ints
START_TEST(test_fifo_as_queue) {
    struct tt_fifo *fifo;
    unsigned last = 0;
    unsigned next = 0;
    unsigned tmp;

    int result = tt_fifo_create(&fifo, (sizeof(unsigned) * 8));
    ck_assert(0 == result);

    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(sizeof(next) == result);
    ++next;
    result = tt_fifo_write(fifo, &next, sizeof(next));
    ck_assert(0 == result);

    for(int i = 0; i < 1000; ++i) {
        result = tt_fifo_read(fifo, &tmp, sizeof(tmp));
        ck_assert(sizeof(tmp) == result);
        ck_assert(tmp == last);
        ++last;
        result = tt_fifo_write(fifo, &next, sizeof(next));
        ck_assert(sizeof(next) == result);
        ++next;
    }

    free(fifo);

} END_TEST

Suite * mempool_suite(void) {
    Suite *s;
    TCase *tc_core;

    s = suite_create("fifo");

    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_fifo_overwrite);
    tcase_add_test(tc_core, test_fifo_overread);
    tcase_add_test(tc_core, test_fifo);
    tcase_add_test(tc_core, test_fifo_as_queue);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int numFailed;
    Suite *s;
    SRunner *sr;

    srand(time(0));

    s = mempool_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    numFailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return numFailed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
