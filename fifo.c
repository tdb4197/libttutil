/*
 * Copyright 2019 Thomas Bottom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

struct tt_fifo {
    uint8_t* start; // addr of first byte 
    uint8_t* end;   // addr of last byte
    uint8_t* wr;    // write pointer
    uint8_t* rd;    // read pointer
    size_t size;    // fifo size
};

int tt_fifo_create(struct tt_fifo **fifo, size_t size) {
    ++size; // increment because capacity is actually size - 1
    *fifo = malloc(sizeof(struct tt_fifo) + size);
    if(NULL == *fifo) return -ENOMEM;

    // fifo buffer starts immediately after struct tt_fifo
    (*fifo)->start = (uint8_t*)((uintptr_t)(*fifo) + sizeof(struct tt_fifo));
    (*fifo)->end   = ((*fifo)->start + size);
    (*fifo)->wr    = (*fifo)->start;
    (*fifo)->rd    = (*fifo)->start;
    (*fifo)->size  = size;

    return 0;
}

#define TT_FIFO_SIZE(len) (sizeof(struct tt_fifo) + (len))
#define TT_FIFO(len, name) \
    static uint8_t (name##_mem) [TT_FIFO_SIZE(len)]; \
    struct tt_fifo * (name) = (struct tt_fifo*) (name##_mem);

// TODO - consider renaming all of these like tt_fifo_static_init to avoid
//        the potential confusion of someone thinking they can go:
//          struct tt_fifo myFifo;
//          ...
//          tt_fifo_init(&myFifo, len);
//        when the correct usage is:
//          TT_FIFO(len, myFifo);
//          ...
//          tt_fifo_init(myFifo);
//        which expands to:
//          static uint8_t myFifo_mem[TT_FIFO_SIZE];
//          struct tt_fifo *myFifo = (struct tt_fifo*)
//          ..
//          tt_fifo_init(myFifo);
void tt_fifo_init(struct tt_fife *fifo, const size_t len) {
    assert(NULL != fifo);
    assert(0 < len);

    fifo->start = (uint8_t*)((uintptr_t)fifo + sizeof(struct tt_fifo));
    fifo->end   = (uint8_t*)((uintptr_t)fifo->start + len);
    fifo->wr    = fifo->start;
    fifo->rd    = fifo->start;
    fifo->size  = len;
}

int tt_fifo_write(struct tt_fifo *fifo, void* src, size_t len) {
    size_t toWrite;
    int written = 0;

TT_FIFO_WRITE_RECURSE: // entry point for tail call elimination
    if(fifo->wr < fifo->rd) toWrite = (fifo->rd - fifo->wr) - 1;
    else {
        toWrite = (fifo->end - fifo->wr);
        if(fifo->rd == fifo->start) --toWrite;
    }

    if(toWrite) {
        if(toWrite > len) toWrite = len;

        memcpy(fifo->wr, src, toWrite);
        written  += toWrite;
        fifo->wr += toWrite;
        if(fifo->wr == fifo->end) fifo->wr = fifo->start;

        if(written < len) {
            len -= written;
            src = (void*)(uint8_t*)src + written;
            goto TT_FIFO_WRITE_RECURSE;
        }
    }
    return written;
}

int tt_fifo_read(struct tt_fifo *fifo, void* dest, size_t len) {
    size_t toRead;
    int read = 0;

TT_FIFO_READ_RECURSE: // entry point for tail call elimination
    if     (fifo->rd < fifo->wr) toRead = fifo->wr  - fifo->rd;
    else if(fifo->rd > fifo->wr) toRead = fifo->end - fifo->rd;
    else                         toRead = 0;

    if(toRead) {
        if(toRead > len) toRead = len;

        memcpy(dest, fifo->rd, toRead);
        read     += toRead;
        fifo->rd += toRead;
        if(fifo->rd == fifo->end) fifo->rd = fifo->start;

        if(read < len) {
            len -= read;
            dest = (void*)(uint8_t*)dest + read;
            goto TT_FIFO_READ_RECURSE;
        }
    }
    return read;
}
