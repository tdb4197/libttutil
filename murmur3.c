// MurmurHash3 was written by Austin Appleby and placed in the public domain.
// This version was ported by Thomas Bottom and is also in the public domain.

#include "stdint.h"

#include "ttutil/ttutil.h"

// Finalization mix - force all bits of a hash block to avalanche
static inline uint32_t fmix32 ( uint32_t h )
{
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;
  return h;
}

uint32_t tt_hash_murmur3_32_x86(const void* data, const uint32_t len, const uint32_t seed) {
  const uint_fast32_t nblocks = len / 4;
  const uint32_t      *blocks = (const uint32_t *)((uintptr_t)data + nblocks*4);
  uint32_t c1 = 0xcc9e2d51;
  uint32_t c2 = 0x1b873593;
  uint32_t h1 = seed;
  uint32_t k1;

  // body
  for(uint_fast32_t i = 0; i < nblocks; ++i) {
    k1 = blocks[i];
    k1 *= c1;
    k1 = tt_rotl32(k1,15);
    k1 *= c2;
    
    h1 ^= k1;
    h1 = tt_rotl32(h1,13); 
    h1 = h1*5+0xe6546b64;
  }

  // tail
  switch(len & 3)
  {
    case 3: k1 ^= ((const uint8_t*)blocks)[2] << 16;
    case 2: k1 ^= ((const uint8_t*)blocks)[1] <<  8;
    case 1: k1 ^= ((const uint8_t*)blocks)[0];
            k1 *= c1; 
            k1  = tt_rotl32(k1,15); 
            k1 *= c2; 
            h1 ^= k1;
  };

  // finalization
  h1 ^= len;
  h1 = fmix32(h1);
  return h1;
}
